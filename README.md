## Cv Converter
This script is used to convert CVs in pdf format to docx file. The script recognizes such parts of CV:

- short summary; 
- experience; 
- education; 
- skills; 
- certificates;
- courses and certifications;

### How to add a new cv part
#### Parsing CV pdf file:
1. Add new value to `CvParts` enum (provide header in pdf and desired header in generated cv).
2. Add new field to `InputCv`.
3. Extend `PartReader` abstract class to parse new part of the cv and set in `InputCv` object.
4. Add new class extending `PartReader` to `PdfReaderModule`.

#### Adding new part to generated docx file:
(Assumed that previous part - parsing CV pdf file  - was completed):

1. Add new Formatter in cvconverter.word.formatters package - it should extend `Formatter` class.

2. Add provider method for new formatter in `WordCvModule` class in cvconverter.module package. Add it to `provideFormatters` method in the same class. Order of formatters in this method is important, as cv parts will appear in generated document in that order.

### How to build, run and generate jar file

Tyoe the following command to build the project:
```
./gradlew build
```
to run it type:
```
./gradlew run --args <PATH_TO_CV>

```
to generate a jar file, which can be found under `<PATH_TO_PROJECT>/build/libs` :
```
./gradlew fatJar
```

### How to run a script

Make sure that you have Java 11 installed. Download a [cvconverter.jar file](https://drive.google.com/open?id=1KGz7CSY_kH_sRSzcxQWY796As1wRneJx). Run it:

`java -jar cvconverter.jar <PATH_TO_CV_PDF>` 

or (if you want to save converted cv in different directory)

`java -jar cvconverter.jar <PATH_TO_CV_PDF> <DIRECTORY_TO_SAVE_CONVERTED_CV>`

e.g. command 

`java -jar cvconverter.jar "/Users/user123/Desktop/test/test_cv.pdf"`

will create converted cv file here `/Users/user123/Desktop/test/test_cv_converted.docx`.
### Future improvements:
- add logger instead of using `System.out.println`
- handle more than just devire cvs;
- add some ML preprocessing that could categorize pdf lines, example of categories EXPERIENCE_HEADER, EXPERIENCE_DESCRIPTION, EDUCATION, NAME, EDUCATION, etc.