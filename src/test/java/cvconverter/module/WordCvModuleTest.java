package cvconverter.module;

import cvconverter.word.formaters.CertificatesFormatter;
import cvconverter.word.formaters.CoursesAndCertificationsFormatter;
import cvconverter.word.formaters.EducationFormatter;
import cvconverter.word.formaters.ExperienceFormatter;
import cvconverter.word.formaters.Formatter;
import cvconverter.word.formaters.NameFormatter;
import cvconverter.word.formaters.ProfileFormatter;
import cvconverter.word.formaters.SkillsFormatter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class WordCvModuleTest {

    @Mock
    private NameFormatter nameFormatter;
    @Mock
    private ProfileFormatter profileFormatter;
    @Mock
    private ExperienceFormatter experienceFormatter;
    @Mock
    private EducationFormatter educationFormatter;
    @Mock
    private CertificatesFormatter certificatesFormatter;
    @Mock
    private CoursesAndCertificationsFormatter coursesAndCertificationsFormatter;
    @Mock
    private SkillsFormatter skillsFormatter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldEnsureCorrectFormattersOrder() {
        // ensuring that formatters are in correct order
        // this order will result in order of CV elements
        final WordCvModule wordCvModule = new WordCvModule();
        List<Formatter> formatters = wordCvModule.provideFormatters(nameFormatter, profileFormatter, experienceFormatter,
                educationFormatter, certificatesFormatter, coursesAndCertificationsFormatter, skillsFormatter);
        assertThat(formatters).containsExactly(nameFormatter, profileFormatter, experienceFormatter, educationFormatter,
                certificatesFormatter, coursesAndCertificationsFormatter, skillsFormatter);
    }
}