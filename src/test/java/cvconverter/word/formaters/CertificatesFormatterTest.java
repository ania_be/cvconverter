package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.SimpleList;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CertificatesFormatterTest {

    private CertificatesFormatter formatter = new CertificatesFormatter();

    @Test
    public void shouldGetCvPart() {
        assertThat(formatter.getCvPart()).isEqualTo(CvParts.CERTIFICATES);
    }

    @Test
    public void shouldFormat() {
        // given
        final SimpleList elements = SimpleList.builder().hasBulletPoints(true)
                .elements(Arrays.asList(DataFixtures.CERTIFICATE_1, DataFixtures.CERTIFICATE_2))
                .build();
        final XWPFDocument document = new XWPFDocument();

        // when
        formatter.format(document, elements);

        // then
        List<XWPFParagraph> actualParagraphs = document.getParagraphs();
        assertThat(actualParagraphs).hasSize(3);
        assertThat(actualParagraphs.get(0).getText()).isEqualTo(String.format("\n%s\n", formatter.getHeader()));
        assertThat(actualParagraphs.get(1).getText()).isEqualTo(DataFixtures.CERTIFICATE_1);
        assertThat(actualParagraphs.get(2).getText()).isEqualTo(DataFixtures.CERTIFICATE_2);
    }

    private static class DataFixtures {
        private static final String CERTIFICATE_1 = "Certificate 1";
        private static final String CERTIFICATE_2 = "Certificate 2";
    }
}