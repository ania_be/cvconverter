package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.Experience;
import cvconverter.model.SimpleList;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ExperienceFormatterTest {

    private ExperienceFormatter formatter = new ExperienceFormatter();

    @Test
    public void shouldGetCvPart() {
        assertThat(formatter.getCvPart()).isEqualTo(CvParts.EXPERIENCE);
    }

    @Test
    public void shouldFormat() {
        // given
        final List<Experience> elements = Arrays.asList(
                Experience.builder().dateCompanyLine(DataFixtures.DATE_COMPANY_LINE_1)
                        .description(SimpleList.builder()
                                .elements(Arrays.asList(DataFixtures.EXP_1_LINE_1, DataFixtures.EXP_1_LINE_2))
                                .hasBulletPoints(true)
                                .build())
                        .title(DataFixtures.TITLE_1)
                        .build(),
                Experience.builder().dateCompanyLine(DataFixtures.DATE_COMPANY_LINE_2)
                        .description(SimpleList.builder()
                                .elements(Arrays.asList(DataFixtures.EXP_2_LINE_1, DataFixtures.EXP_2_LINE_2))
                                .hasBulletPoints(true)
                                .build())
                        .title(DataFixtures.TITLE_2)
                        .build()

        );
        final XWPFDocument document = new XWPFDocument();

        // when
        formatter.format(document, elements);

        // then
        List<XWPFParagraph> actualParagraphs = document.getParagraphs();
        assertThat(actualParagraphs).hasSize(11);
        assertThat(actualParagraphs.get(0).getText()).isEqualTo(String.format("%s", formatter.getHeader()));
        assertThat(actualParagraphs.get(1).getText()).isEqualTo(String.format("%s, %s", DataFixtures.TITLE_1, DataFixtures.DATE_COMPANY_LINE_1));
        assertEmptyParagraph(actualParagraphs.get(2));
        assertThat(actualParagraphs.get(3).getText()).isEqualTo(DataFixtures.EXP_1_LINE_1);
        assertThat(actualParagraphs.get(4).getText()).isEqualTo(DataFixtures.EXP_1_LINE_2);
        assertEmptyParagraph(actualParagraphs.get(5));
        assertThat(actualParagraphs.get(6).getText()).isEqualTo(String.format("%s, %s", DataFixtures.TITLE_2, DataFixtures.DATE_COMPANY_LINE_2));
        assertEmptyParagraph(actualParagraphs.get(7));
        assertThat(actualParagraphs.get(8).getText()).isEqualTo(DataFixtures.EXP_2_LINE_1);
        assertThat(actualParagraphs.get(9).getText()).isEqualTo(DataFixtures.EXP_2_LINE_2);
        assertEmptyParagraph(actualParagraphs.get(10));
    }

    private void assertEmptyParagraph(final XWPFParagraph p) {
        assertThat(p.getText()).isEmpty();
    }

    private static class DataFixtures {
        private static final String DATE_COMPANY_LINE_1 = "Date company line 1";
        private static final String EXP_1_LINE_1 = "element1";
        private static final String EXP_1_LINE_2 = "element2";
        private static final String TITLE_1 = "title 1";
        private static final String TITLE_2 = "title 2";
        private static final String DATE_COMPANY_LINE_2 = "Date company line 2";
        private static final String EXP_2_LINE_1 = "element3";
        private static final String EXP_2_LINE_2 = "element4";
    }

}