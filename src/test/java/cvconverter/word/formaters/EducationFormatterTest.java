package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.Education;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class EducationFormatterTest {

    private EducationFormatter formatter = new EducationFormatter();

    @Test
    public void shouldGetTheCvPart() {
        assertThat(formatter.getCvPart()).isEqualTo(CvParts.EDUCATION);
    }

    @Test
    public void shouldFormat() {
        // given
        final List<Education> elements = Arrays.asList(
                Education.builder().description(Arrays.asList(DataFixtures.LINE_1, DataFixtures.LINE_2))
                        .start(DataFixtures.START_1)
                        .end(DataFixtures.END_1)
                        .build(),
                Education.builder().description(Arrays.asList(DataFixtures.LINE_3, DataFixtures.LINE_4))
                        .start(DataFixtures.START_2)
                        .end(DataFixtures.END_2)
                        .build());
        final XWPFDocument document = new XWPFDocument();

        // when
        formatter.format(document, elements);

        // then
        final List<XWPFParagraph> actualParagraphs = document.getParagraphs();
        assertThat(actualParagraphs).hasSize(5);
        assertThat(actualParagraphs.get(0).getText()).isEqualTo(formatter.getHeader());
        assertThat(actualParagraphs.get(1).getText()).isEqualTo(String.format("%s %s, %s - %s",
                DataFixtures.LINE_1, DataFixtures.LINE_2, DataFixtures.START_1, DataFixtures.END_1));
        assertNewLineParagraph(actualParagraphs.get(2));
        assertThat(actualParagraphs.get(3).getText()).isEqualTo(String.format("%s %s, %s - %s",
                DataFixtures.LINE_3, DataFixtures.LINE_4, DataFixtures.START_2, DataFixtures.END_2));
        assertNewLineParagraph(actualParagraphs.get(4));
    }

    private void assertNewLineParagraph(final XWPFParagraph p) {
        assertThat(p.getText()).isEqualTo("\n");
    }

    private static class DataFixtures {
        private static final String LINE_1 = "line1";
        private static final String LINE_2 = "line2";
        private static final String LINE_3 = "line3";
        private static final String LINE_4 = "line4";
        private static final String START_1 = "2012";
        private static final String END_1 = "2014";
        private static final String START_2 = "2014";
        private static final String END_2 = "2015";
    }
}