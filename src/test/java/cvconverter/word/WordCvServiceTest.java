package cvconverter.word;

import cvconverter.model.InputCv;
import cvconverter.word.formaters.Formatter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class WordCvServiceTest {

    private static final String TEST_OUTPUT_PATH = "test_output_path";

    @Mock
    private Formatter mockFormatter1;

    @Mock
    private Formatter mockFormatter2;

    @Mock
    private WordFileService mockWordFileService;

    @Mock
    private InputCv mockInputCv;

    private WordCvService wordCvService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        wordCvService = new WordCvService(Arrays.asList(mockFormatter1, mockFormatter2), mockWordFileService);
    }

    @Test
    public void shouldGenerateCv() throws Exception {
        wordCvService.generateCv(mockInputCv, TEST_OUTPUT_PATH);

        verifyFormatIfPresentCall(mockFormatter1);
        verifyFormatIfPresentCall(mockFormatter2);
        verify(mockWordFileService).save(eq(TEST_OUTPUT_PATH), any(XWPFDocument.class));
    }

    private void verifyFormatIfPresentCall(Formatter f) {
        verify(f).formatIfPresent(any(XWPFDocument.class), eq(mockInputCv));
    }
}