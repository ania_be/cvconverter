package cvconverter;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CvConverterTest {

    private static final String CV_FILE_PATH = "/Users/testUser/Desktop/test_cv.pdf";
    private static final String CV_FILE_PATH_NO_PDF = "/Users/testUser/Desktop/test_cv";
    private static final String OUTPUT_FILE_PATH_FROM_CV = "/Users/testUser/Desktop/test_cv_converted.docx";
    private static final String OUTPUT_PATH = "/Users/testUser/Documents";
    private static final String OUTPUT_FILE_PATH = "/Users/testUser/Documents/test_cv_converted.docx";

    @Test
    public void shouldGetInputPath() {
        String inputPath = CvConverter.getInputPath(new String[]{CV_FILE_PATH});
        assertThat(inputPath).isEqualTo(inputPath);
    }

    @Test
    public void shouldGetOutputPathFromOneArgument() {
        String outputPath = CvConverter.getOutputPath(new String[]{CV_FILE_PATH});
        assertThat(outputPath).isEqualTo(OUTPUT_FILE_PATH_FROM_CV);
    }

    @Test
    public void shouldGetOutputPathFromTwoArguments() {
        String outputPath = CvConverter.getOutputPath(new String[]{CV_FILE_PATH, OUTPUT_PATH});
        assertThat(outputPath).isEqualTo(OUTPUT_FILE_PATH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForNoArguments() {
        CvConverter.getInputPath(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfInputNotPdf() {
        CvConverter.getInputPath(new String[]{CV_FILE_PATH_NO_PDF});
    }
}