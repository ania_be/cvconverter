package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ShortSummaryReaderTest extends ReaderTestBase {

    private ShortSummaryReader shortSummaryReader = new ShortSummaryReader();

    @Test
    public void shouldReadShortSummary() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "Short summary", "first line",
                " second line", "last line", "EDUCATION", "lines after should not be included");
        final String summary = shortSummaryReader.readAndSet(pdfLines, cvBuilder);
        assertThat(summary).isEqualTo("first line second line last line");
    }

    @Test(expected = PartNotFoundException.class)
    public void shouldThrowPartNotFoundException() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "Experience", "first line",
                " second line", "last line", "EDUCATION", "lines after should not be included");
        shortSummaryReader.readAndSet(pdfLines, cvBuilder);
    }
}
