package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.SimpleList;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CoursesAndCertificationsReaderTest extends ReaderTestBase {
    
    private final CoursesAndCertificationsReader reader = new CoursesAndCertificationsReader();

    @Test
    public void shouldReadCoursesAndCertifications() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "COURSES AND CERTIFICATIONS", " - first certificate",
                " second line of first certificate", " - second certificate", "-third certificate", "end of third line",
                "EDUCATION", "lines after should not be included");
        final SimpleList simpleList = reader.readAndSet(pdfLines, cvBuilder);
        assertThat(simpleList.getElements()).isEqualTo(Arrays.asList("first certificate second line of first certificate",
                "second certificate", "third certificate end of third line"));
        assertThat(simpleList.getHasBulletPoints()).isTrue();
        assertThat(simpleList.getIndexAfter()).isEqualTo(7L);
    }

    @Test(expected = PartNotFoundException.class)
    public void shouldThrowPartNotFoundException() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "test", " - first certificate",
                " second line of first certificate", " - second certificate", "-third certificate", "end of third line",
                "education", "lines after should not be included");
        reader.readAndSet(pdfLines, cvBuilder);
    }

}