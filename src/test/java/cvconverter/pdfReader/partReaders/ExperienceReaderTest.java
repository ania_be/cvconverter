package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.Experience;
import cvconverter.model.SimpleList;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ExperienceReaderTest extends ReaderTestBase {

    private ExperienceReader experienceReader = new ExperienceReader();

    @Test
    public void shouldReadExperience() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "EXPERIENCE",
                "04.2015 – 05.2016 Some Random Bank", "SOFTWARE ENGINEER",
                "- Building organization core product allowing to securely identify"," and authenticate customers",
                "- Building live fraud detection platform that mitigates malicious", "activity",
                "July 2017 – Present Test Company SP. Z.O.O", "Business Analyst",
                "- Identification of customer needs of the business and its customers",
                "- Creating documentation - requirements, technical, user",
                "documentation, detailed functionalities descriptions for the", "developers",
                "Short summary", "lines after should not be included");
        final List<Experience> experiences = experienceReader.readAndSet(pdfLines, cvBuilder);
        assertThat(experiences).hasSize(2);
        assertExperience(experiences.get(0), "SOFTWARE ENGINEER", "04.2015 – 05.2016 Some Random Bank", 8,
                Arrays.asList("Building organization core product allowing to securely identify and authenticate customers",
                        "Building live fraud detection platform that mitigates malicious activity"), true);
        assertExperience(experiences.get(1), "Business Analyst", "July 2017 – Present Test Company SP. Z.O.O", 14,
                Arrays.asList("Identification of customer needs of the business and its customers",
                        "Creating documentation - requirements, technical, user documentation, detailed functionalities descriptions for the developers"),
                true);
    }


    @Test(expected = PartNotFoundException.class)
    public void shouldThrowPartNotFoundException() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "test", " - first certificate",
                " second line of first certificate", " - second certificate", "-third certificate", "end of third line",
                "education", "lines after should not be included");
        experienceReader.readAndSet(pdfLines, cvBuilder);
    }

    private void assertExperience(final Experience actualExperience,
                                  final String expectedTitle,
                                  final String expectedCompanyDate,
                                  final int expectedIndexAfter,
                                  final List<String> expectedElements,
                                  final boolean expectedHasBulletPoints) {
        assertThat(actualExperience.getTitle()).isEqualTo(expectedTitle);
        assertThat(actualExperience.getDateCompanyLine()).isEqualTo(expectedCompanyDate);
        SimpleList description = actualExperience.getDescription();
        assertThat(description.getIndexAfter()).isEqualTo(expectedIndexAfter);
        assertThat(description.getHasBulletPoints()).isEqualTo(expectedHasBulletPoints);
        assertThat(description.getElements()).isEqualTo(expectedElements);
    }
}