package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.SimpleList;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SkillsReaderTest extends ReaderTestBase {

    private SkillsReader skillsReader = new SkillsReader();

    @Test
    public void shouldReadListedSkills() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "SKILLS", " - first line",
                " second line of first", " - second line", "-third line", "end of third line",
                "EDUCATION", "lines after should not be included");
        SimpleList listOfSkills = skillsReader.readAndSet(pdfLines, cvBuilder);
        List<String> elements = listOfSkills.getElements();
        assertThat(elements).hasSize(3);
        assertThat(elements.get(0)).isEqualTo("first line second line of first");
        assertThat(elements.get(1)).isEqualTo("second line");
        assertThat(elements.get(2)).isEqualTo("third line end of third line");
        assertThat(listOfSkills.getHasBulletPoints()).isTrue();
    }

    @Test
    public void shouldReadWholeTextSkills() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "SKILLS", "test text",
                "second line", "third line", "EDUCATION", "lines after should not be included");
        SimpleList listOfSkills = skillsReader.readAndSet(pdfLines, cvBuilder);
        assertThat(listOfSkills.getElements()).hasSize(1);
        assertThat(listOfSkills.getElements().get(0)).isEqualTo("test text second line third line");
        assertThat(listOfSkills.getHasBulletPoints()).isFalse();
    }

    @Test(expected = PartNotFoundException.class)
    public void shouldBeEmptyIfNoSkills() throws Exception {
        List<String> pdfLines = Arrays.asList("should not be included", "text", "second line", "third line");
        skillsReader.readAndSet(pdfLines, cvBuilder);
    }
}
