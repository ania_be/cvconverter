package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.Education;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class EducationReaderTest extends ReaderTestBase {

    private final EducationReader educationReader = new EducationReader();

    @Test
    public void shouldReadEducation() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "EDUCATION", " - first university 2014 - 2015",
                " second line of first certificate", " - second certificate 2015-2016", "-third university 2008 - 2009", "end of third line",
                "SKILLS", "lines after should not be included");
        final List<Education> educationList = educationReader.readAndSet(pdfLines, cvBuilder);
        assertThat(educationList.size()).isEqualTo(3);
        assertEducation(educationList.get(0), "2014","2015",
                Arrays.asList("- first university 2014 - 2015", " second line of first certificate"));
        assertEducation(educationList.get(1), "2015","2016",
                Collections.singletonList("- second certificate 2015-2016"));
        assertEducation(educationList.get(2), "2008","2009",
                Arrays.asList("-third university 2008 - 2009", "end of third line"));
    }

    @Test(expected = PartNotFoundException.class)
    public void shouldThrowPartNotFoundException() throws Exception {
        final List<String> pdfLines = Arrays.asList("should not be included", "test", " - first certificate",
                " second line of first certificate", " - second certificate", "-third certificate", "end of third line",
                "skills", "lines after should not be included");
        educationReader.readAndSet(pdfLines, cvBuilder);
    }

    private void assertEducation(final Education actual,
                                 final String expectedStart,
                                 final String expectedEnd,
                                 final List<String> expectedDescription) {
        assertThat(actual.getStart()).isEqualTo(expectedStart);
        assertThat(actual.getEnd()).isEqualTo(expectedEnd);
        assertThat(actual.getDescription()).isEqualTo(expectedDescription);
    }
}