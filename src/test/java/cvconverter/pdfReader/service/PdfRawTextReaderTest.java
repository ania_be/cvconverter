package cvconverter.pdfReader.service;

import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PdfRawTextReaderTest {

    private PdfRawTextReader pdfRawTextReader = new PdfRawTextReader();

    @Test
    public void shouldGetPfLines() throws Exception {
        List<String> pdfLines = pdfRawTextReader.getPdfLines(DataFixtures.TEST_PDF);
        assertThat(pdfLines).isNotEmpty();
    }

    @Test(expected = InvalidPasswordException.class)
    public void shouldThrowInvalidPasswordException() throws Exception {
        pdfRawTextReader.getPdfLines(DataFixtures.TEST_PDF_WITH_PASSWORD);
    }

    private static class DataFixtures {
        private static final String TEST_PDF = "src/test/resources/testPdf.pdf";
        private static final String TEST_PDF_WITH_PASSWORD = "src/test/resources/withPassword.pdf";
    }
}