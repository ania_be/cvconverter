package cvconverter.pdfReader.service;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SanitizerServiceTest {

    private SanitizerService service = new SanitizerService();

    @Test
    public void shouldSanitizeLines() {
        final List<String> strings = service.sanitizePdfLines(
                Arrays.asList("CV", "test", "www.devire.pl", "test2", "vitae", "Curriculum", "test3"));
        assertThat(strings).containsExactly("test", "test2", "test3");
    }

    @Test
    public void shouldKeepAllLines() {
        final List<String> pdfLines = Arrays.asList("test1", "test2");
        final List<String> actualLines = service.sanitizePdfLines(pdfLines);
        assertThat(actualLines).isEqualTo(pdfLines);
    }
}
