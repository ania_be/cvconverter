package cvconverter.pdfReader;

import cvconverter.model.InputCv;
import cvconverter.pdfReader.partReaders.PartReader;
import cvconverter.pdfReader.service.PdfRawTextReader;
import cvconverter.pdfReader.service.SanitizerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PdfParserTest {

    @Mock
    private SanitizerService mockSanitizer;

    @Mock
    private PartReader mockReader1;

    @Mock
    private PartReader mockReader2;

    @Mock
    private PdfRawTextReader mockRawTextReader;

    private PdfParser pdfParser;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        pdfParser = new PdfParser(Arrays.asList(mockReader1, mockReader2), mockSanitizer, mockRawTextReader);
    }

    @Test
    public void shouldReadCv() throws Throwable {
        // given
        when(mockRawTextReader.getPdfLines(DataFixtures.TEST_FILE_PATH)).thenReturn(DataFixtures.PDF_LINES);
        when(mockSanitizer.sanitizePdfLines(anyListOf(String.class))).thenReturn(DataFixtures.PDF_LINES);

        // when
        final InputCv inputCv = pdfParser.readCv(DataFixtures.TEST_FILE_PATH);

        // then
        assertThat(inputCv).isNotNull();
        verify(mockSanitizer).sanitizePdfLines(eq(DataFixtures.PDF_LINES));
        verify(mockReader1).readAndSetIfPresent(eq(DataFixtures.PDF_LINES), any(InputCv.InputCvBuilder.class));
        verify(mockReader2).readAndSetIfPresent(eq(DataFixtures.PDF_LINES), any(InputCv.InputCvBuilder.class));
    }

    private static class DataFixtures {
        private static final String TEST_FILE_PATH = "testPath";
        private static final List<String> PDF_LINES = Arrays.asList("test", "test2");
    }
}