package cvconverter.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import cvconverter.pdfReader.partReaders.CertificatesReader;
import cvconverter.pdfReader.partReaders.CoursesAndCertificationsReader;
import cvconverter.pdfReader.partReaders.EducationReader;
import cvconverter.pdfReader.partReaders.ExperienceReader;
import cvconverter.pdfReader.partReaders.PartReader;
import cvconverter.pdfReader.partReaders.ShortSummaryReader;
import cvconverter.pdfReader.partReaders.SkillsReader;
import cvconverter.pdfReader.service.PdfRawTextReader;
import cvconverter.pdfReader.service.SanitizerService;

import java.util.Arrays;
import java.util.List;

public class PdfReaderModule extends AbstractModule {

    @Provides
    public List<PartReader> readers(final SkillsReader skillsReader,
                                    final ShortSummaryReader shortSummaryReader,
                                    final ExperienceReader experienceReader,
                                    final EducationReader educationReader,
                                    final CoursesAndCertificationsReader coursesAndCertificationsReader,
                                    final CertificatesReader certificatesReader) {
        return Arrays.asList(skillsReader, shortSummaryReader, experienceReader, educationReader,
                coursesAndCertificationsReader, certificatesReader);
    }

    @Provides
    public SkillsReader provideSkillsReader() {
        return new SkillsReader();
    }

    @Provides
    public ShortSummaryReader provideShortSummaryReader() {
        return new ShortSummaryReader();
    }

    @Provides
    public ExperienceReader provideExperienceReader() {
        return new ExperienceReader();
    }

    @Provides
    public EducationReader provideEducationReader() {
        return new EducationReader();
    }

    @Provides
    public CoursesAndCertificationsReader provideCoursesAndCertificationsReader() {
        return new CoursesAndCertificationsReader();
    }

    @Provides
    public CertificatesReader provideCertificatesReader() {
        return new CertificatesReader();
    }

    @Provides
    public SanitizerService provideSanitizerService() {
        return new SanitizerService();
    }

    @Provides
    public PdfRawTextReader providePdfRawTextReader() {
        return new PdfRawTextReader();
    }
}
