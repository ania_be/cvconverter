package cvconverter.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import cvconverter.word.WordFileService;
import cvconverter.word.formaters.CertificatesFormatter;
import cvconverter.word.formaters.CoursesAndCertificationsFormatter;
import cvconverter.word.formaters.EducationFormatter;
import cvconverter.word.formaters.ExperienceFormatter;
import cvconverter.word.formaters.Formatter;
import cvconverter.word.formaters.NameFormatter;
import cvconverter.word.formaters.ProfileFormatter;
import cvconverter.word.formaters.SkillsFormatter;

import java.util.Arrays;
import java.util.List;

public class WordCvModule extends AbstractModule {

    @Provides
    public WordFileService provideWordFileService() {
        return new WordFileService();
    }

    @Provides
    public List<Formatter> provideFormatters(final NameFormatter nameFormatter,
                                             final ProfileFormatter profileFormatter,
                                             final ExperienceFormatter experienceFormatter,
                                             final EducationFormatter educationFormatter,
                                             final CertificatesFormatter certificatesFormatter,
                                             final CoursesAndCertificationsFormatter coursesAndCertificationsFormatter,
                                             final SkillsFormatter skillsFormatter) {
        // order of the formatters is important - parts of generated cv will appear in that order
        return Arrays.asList(nameFormatter,
                profileFormatter,
                experienceFormatter,
                educationFormatter,
                certificatesFormatter,
                coursesAndCertificationsFormatter,
                skillsFormatter);
    }

    @Provides
    public NameFormatter provideNameFormatter() {
        return new NameFormatter();
    }

    @Provides
    public ProfileFormatter provideProfileFormatter() {
        return new ProfileFormatter();
    }

    @Provides
    public CertificatesFormatter provideCertificatesFormatter() {
        return new CertificatesFormatter();
    }

    @Provides
    public CoursesAndCertificationsFormatter provideCoursesAndCertificationsFormatter() {
        return new CoursesAndCertificationsFormatter();
    }

    @Provides
    public EducationFormatter provideEducationFormatter() {
        return new EducationFormatter();
    }

    @Provides
    public ExperienceFormatter provideExperienceFormatter () {
        return new ExperienceFormatter();
    }

    @Provides
    public SkillsFormatter provideSkillsFormatter () {
        return new SkillsFormatter();
    }
}
