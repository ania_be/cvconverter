package cvconverter.exception;

import cvconverter.model.CvParts;

public class PartNotFoundException extends Exception {
    public PartNotFoundException(CvParts parts) {
        super(String.format("Part %s not found.", parts.getPartName()));
    }
}
