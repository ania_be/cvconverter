package cvconverter;

import com.google.inject.Guice;
import com.google.inject.Injector;
import cvconverter.exception.PdfEncryptedException;
import cvconverter.model.InputCv;
import cvconverter.module.PdfReaderModule;
import cvconverter.module.WordCvModule;
import cvconverter.pdfReader.PdfParser;
import cvconverter.word.WordCvService;

import java.io.IOException;

public class CvConverter {

    private static final String CONVERTED_SUFFIX = "_converted.docx";

    public static void main(String[] args) {
        // read CV from pdf to
        Injector injector = Guice.createInjector(new PdfReaderModule(), new WordCvModule());
        PdfParser pdfParser = injector.getInstance(PdfParser.class);
        WordCvService wordCvService = injector.getInstance(WordCvService.class);

        try {
            final String inputFile  = getInputPath(args);
            final String outputFile  = getOutputPath(args);
            System.out.println(String.format("Reading cv from: %s", inputFile));
            System.out.println(String.format("Converted cv file: %s", outputFile));

            InputCv inputCv = pdfParser.readCv(inputFile);
            wordCvService.generateCv(inputCv, outputFile);
        } catch (IOException | PdfEncryptedException e) {
            System.out.println(String.format("Could not read pdf due to exception %s and message: %s",
                    e.getClass().getSimpleName(), e.getMessage()));
        }
    }

    static String getOutputPath(String[] args) {
        if(args.length == 1) {
            System.out.println("No path to converted cv specified. File will be generated in the same location");
            String original = args[0];
            return String.format("%s%s", original.substring(0, original.lastIndexOf(".")), CONVERTED_SUFFIX);
        }
        return String.format("%s/%s%s",
                args[1],
                args[0].substring(args[0].lastIndexOf("/") + 1, args[0].lastIndexOf(".")),
                CONVERTED_SUFFIX);
    }


    static String getInputPath(String[] args) {
        if(args.length == 0) {
            throw new IllegalArgumentException("No arguments specified. Please provide at least path to the cv.");
        }
        if(!args[0].endsWith(".pdf")) {
            throw new IllegalArgumentException(String.format("Cv should be in pdf format. Found this path instead: %s", args[0]));
        }
        return args[0];
    }
}
