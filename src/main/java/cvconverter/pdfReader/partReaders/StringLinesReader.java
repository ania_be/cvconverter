package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.SimpleList;
import cvconverter.util.LineReader;

import java.util.List;

abstract class StringLinesReader extends PartReader<SimpleList> {

    SimpleList read(final List<String> pdfLines, CvParts part) throws PartNotFoundException {
         int currentIndex = findPartStartLine(pdfLines, part) + 1;
        if (currentIndex == 0) {
            throw new PartNotFoundException(part);
        } else {
            return LineReader.read(pdfLines, currentIndex, this::isNotPdfEndAndNotNewPart);
        }
    }
}
