package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.DatePatterns;
import cvconverter.model.InputCv;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public abstract class PartReader<T> {

    static final String SPACE = " ";

    public void readAndSetIfPresent(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder) {
        try {
            readAndSet(pdfLines, cvBuilder);
        } catch (PartNotFoundException ex) {
            System.out.println(String.format("[PDF READING] CV does not contain part %s", getCvPart().getPartName()));
        } catch (Exception ex) {
            System.out.println(String.format(
                    "Could not process CV part %s. Please past this part manually. Exception: %s and message %s.",
                    getCvPart().getPartName(), ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    abstract T readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder) throws PartNotFoundException;

    abstract CvParts getCvPart();

    boolean isNotPdfEndAndNotNewPart(List<String> pdfLines, int currentIndex) {
        return currentIndex < pdfLines.size() && !isNewPart(pdfLines.get(currentIndex));
    }

    boolean isNotEndNotNewPartNotNewEntryWithDate(List<String> pdfLines, int nextIndex) {
        return isNotPdfEndAndNotNewPart(pdfLines, nextIndex) && !hasLineWithDate(pdfLines.get(nextIndex));
    }

    private boolean hasLineWithDate(String line) {
        return Stream.of(DatePatterns.values())
                .map(DatePatterns::getDatePattern)
                .anyMatch(pattern -> pattern.matcher(line).find());
    }

    private boolean isNewPart(String line) {
        return Stream.of(CvParts.values()).map(CvParts::getPartName).anyMatch(part -> part.equals(line.strip()));
    }

    int findPartStartLine(List<String> pdfLines, CvParts part) {
        return IntStream.range(0, pdfLines.size())
                .filter(i -> pdfLines.get(i).contains(part.getPartName()))
                .findFirst()
                .orElse(-1);
    }
}
