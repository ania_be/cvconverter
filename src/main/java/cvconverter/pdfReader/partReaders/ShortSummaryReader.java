package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import lombok.extern.log4j.Log4j2;

import java.util.List;

import static cvconverter.model.CvParts.SHORT_SUMMARY;

@Log4j2
public class ShortSummaryReader extends PartReader<String> {

    public String readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder)
            throws PartNotFoundException {
            int currentIndex = findPartStartLine(pdfLines, getCvPart()) + 1;
            if(currentIndex == 0) {
               throw new PartNotFoundException(getCvPart());
            }
            StringBuilder sb = new StringBuilder();
            while(isNotPdfEndAndNotNewPart(pdfLines, currentIndex)) {
                sb.append(pdfLines.get(currentIndex++).strip());
                sb.append(SPACE);
            }
        final String shortSummary = sb.toString().strip();
        cvBuilder.shortSummary(shortSummary);
        return shortSummary;
    }

    @Override
    CvParts getCvPart() {
        return SHORT_SUMMARY;
    }
}
