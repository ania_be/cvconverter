package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.Experience;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;
import cvconverter.util.LineReader;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

import static cvconverter.model.CvParts.EXPERIENCE;

@Log4j2
public class ExperienceReader extends PartReader<List<Experience>> {

    public List<Experience> readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder)
            throws PartNotFoundException {
        int currentIndex = findPartStartLine(pdfLines, getCvPart()) + 1;
        final List<Experience> experiences = new ArrayList<>();
        if(currentIndex == 0) {
            throw new PartNotFoundException(getCvPart());
        }
        while(isNotPdfEndAndNotNewPart(pdfLines, currentIndex)) {
            currentIndex = addNewExperience(pdfLines, currentIndex, experiences);
        }
        cvBuilder.experiences(experiences);
        return experiences;
    }

    @Override
    CvParts getCvPart() {
        return EXPERIENCE;
    }

    private int addNewExperience(List<String> pdfLines, int nextIndex, List<Experience> experiences) {
        Experience.ExperienceBuilder experienceBuilder = Experience.builder();
        experienceBuilder.dateCompanyLine(pdfLines.get(nextIndex++).trim());
        experienceBuilder.title(pdfLines.get(nextIndex++).trim());
        final SimpleList description = LineReader.read(pdfLines, nextIndex, this::isNotEndNotNewPartNotNewEntryWithDate);
        experienceBuilder.description(description);
        experiences.add(experienceBuilder.build());
        return description.getIndexAfter();
    }
}
