package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.DatePatterns;
import cvconverter.model.Education;
import cvconverter.model.InputCv;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cvconverter.model.CvParts.EDUCATION;

@Log4j2
public class EducationReader extends PartReader<List<Education>> {

    private static Pattern EDUCATION_DATE = Pattern.compile("\\d{4} ?- ?\\d{4}");

    public List<Education> readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder)
            throws PartNotFoundException {
        int currentIndex = findPartStartLine(pdfLines, getCvPart()) + 1;
        final List<Education> educations = new ArrayList<>();
        if(currentIndex == 0) {
            throw new PartNotFoundException(getCvPart());
        }
        while(isNotPdfEndAndNotNewPart(pdfLines, currentIndex)) {
            currentIndex = addNewEducation(pdfLines, currentIndex, educations);
        }
        cvBuilder.education(educations);
        return educations;
    }

    @Override
    CvParts getCvPart() {
        return EDUCATION;
    }

    private int addNewEducation(List<String> pdfLines, int nextIndex, List<Education> educations) {
        // extract start and end date from first line
        String firstEducationLine = pdfLines.get(nextIndex);
        Matcher matcher = EDUCATION_DATE.matcher(firstEducationLine);
        matcher.find();
        String group = matcher.group();

        Matcher yearMatcher = DatePatterns.YEAR_ONLY.getDatePattern().matcher(group);
        Education.EducationBuilder builder = Education.builder();
        yearMatcher.find();
        builder.start(yearMatcher.group());
        yearMatcher.find();
        builder.end(yearMatcher.group());
        List<String> description = new ArrayList<>();
        description.add(StringUtils.removeStart(firstEducationLine, group).trim());
        nextIndex++;
        while(isNotEndNotNewPartNotNewEntryWithDate(pdfLines, nextIndex)) {
            description.add(pdfLines.get(nextIndex++));
        }
        builder.description(description);
        educations.add(builder.build());
        return nextIndex;
    }
}
