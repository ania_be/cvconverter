package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;

import java.util.List;

import static cvconverter.model.CvParts.COURSES_AND_CERTIFICATIONS;

public class CoursesAndCertificationsReader extends StringLinesReader {
    public SimpleList readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder)
            throws PartNotFoundException {
        final SimpleList result = this.read(pdfLines, getCvPart());
        cvBuilder.coursesAndCertifications(result);
        return result;
    }

    @Override
    CvParts getCvPart() {
        return COURSES_AND_CERTIFICATIONS;
    }
}
