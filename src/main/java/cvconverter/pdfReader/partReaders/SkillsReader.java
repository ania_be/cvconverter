package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;

import java.util.List;

import static cvconverter.model.CvParts.SKILLS;

public class SkillsReader extends StringLinesReader {
    public SimpleList readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder)
            throws PartNotFoundException {
        final SimpleList skills = this.read(pdfLines, getCvPart());
        cvBuilder.skills(skills);
        return skills;
    }

    @Override
    CvParts getCvPart() {
        return SKILLS;
    }
}
