package cvconverter.pdfReader.partReaders;

import cvconverter.exception.PartNotFoundException;
import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;

import java.util.List;

import static cvconverter.model.CvParts.CERTIFICATES;

public class CertificatesReader extends StringLinesReader {
    public SimpleList readAndSet(final List<String> pdfLines, final InputCv.InputCvBuilder cvBuilder) throws PartNotFoundException {
        SimpleList result = this.read(pdfLines, getCvPart());
        cvBuilder.certificates(result);
        return result;
    }

    @Override
    CvParts getCvPart() {
        return CERTIFICATES;
    }
}
