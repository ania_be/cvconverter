package cvconverter.pdfReader;

import com.google.inject.Inject;
import cvconverter.exception.PdfEncryptedException;
import cvconverter.model.InputCv;
import cvconverter.pdfReader.partReaders.PartReader;
import cvconverter.pdfReader.service.PdfRawTextReader;
import cvconverter.pdfReader.service.SanitizerService;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.List;

@Log4j2
public class PdfParser {

    private SanitizerService sanitizerService;

    private List<PartReader> readers;

    private PdfRawTextReader pdfRawTextReader;

    @Inject
    public PdfParser(final List<PartReader> readers,
                     final SanitizerService sanitizerService,
                     final PdfRawTextReader pdfRawTextReader) {
        this.readers = readers;
        this.sanitizerService = sanitizerService;
        this.pdfRawTextReader = pdfRawTextReader;
    }

    /**
     * Reads text from pdf file and parses it to InputCv object.
     *
     * @param pdfPath path to pdf cv
     * @return cv parsed to InputCv object
     */
    public InputCv readCv(final String pdfPath) throws IOException, PdfEncryptedException {
        List<String> pdfLines = pdfRawTextReader.getPdfLines(pdfPath);
        List<String> sanitizedLines = sanitizerService.sanitizePdfLines(pdfLines);
        return linesToCv(sanitizedLines);
    }

    private InputCv linesToCv(final List<String> pdfLines) {
        // FIXME try to make better guess on the name
        InputCv.InputCvBuilder cvBuilder = InputCv.builder()
                .name(pdfLines.get(0))
                .title(pdfLines.get(1));
        readers.forEach(reader -> reader.readAndSetIfPresent(pdfLines, cvBuilder));
        return cvBuilder.build();
    }
}
