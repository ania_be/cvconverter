package cvconverter.pdfReader.service;

import cvconverter.exception.PdfEncryptedException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PdfRawTextReader {

    public List<String> getPdfLines(final String pdfPath) throws IOException, PdfEncryptedException {
        try (PDDocument document = PDDocument.load(new File(pdfPath))) {
            if (!document.isEncrypted() && !document.isAllSecurityToBeRemoved()) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();
                String pdfFileInText = tStripper.getText(document);
                // split by whitespace
                return Arrays.asList(pdfFileInText.split("\\r?\\n"));
            } else {
                throw new PdfEncryptedException();
            }
        }
    }
}
