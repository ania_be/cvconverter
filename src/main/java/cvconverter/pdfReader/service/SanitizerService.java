package cvconverter.pdfReader.service;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SanitizerService {

    private static final ImmutableSet<String> PHRASES_TO_REMOVE = ImmutableSet.of("www.devire.pl",
            "cv",
            "curriculum",
            "vitae");

    // Removes blank lines (null, empty or white characters) and website address
    public List<String> sanitizePdfLines(List<String> pdfLines) {
        return pdfLines.stream()
                .filter(line -> (!StringUtils.isBlank(line) && !containsPhraseToRemove(line)))
                .collect(Collectors.toList());
    }

    private boolean containsPhraseToRemove(final String line) {
        return PHRASES_TO_REMOVE.stream().anyMatch(p -> line.toLowerCase().contains(p));
    }
}
