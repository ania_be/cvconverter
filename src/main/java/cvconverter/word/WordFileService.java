package cvconverter.word;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileOutputStream;
import java.io.IOException;

public class WordFileService {

    void save(final String outputPath, final XWPFDocument document) throws IOException {
        FileOutputStream out = new FileOutputStream(outputPath);
        document.write(out);
        out.close();
        document.close();
    }
}
