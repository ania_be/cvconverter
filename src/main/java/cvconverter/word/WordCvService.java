package cvconverter.word;

import com.google.inject.Inject;
import cvconverter.model.InputCv;
import cvconverter.word.formaters.Formatter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.util.List;

public class WordCvService {

    private List<Formatter> formatters;
    private WordFileService wordFileService;

    @Inject
    public WordCvService(final List<Formatter> formatters,
                         final WordFileService wordFileService) {
        this.formatters = formatters;
        this.wordFileService = wordFileService;
    }

    public void generateCv(final InputCv inputCv, final String outputPath) throws IOException {
        final XWPFDocument document = new XWPFDocument();
        formatters.forEach(f -> f.formatIfPresent(document, inputCv));
        wordFileService.save(outputPath, document);
    }
}
