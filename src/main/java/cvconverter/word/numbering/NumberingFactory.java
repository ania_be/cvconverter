package cvconverter.word.numbering;

import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTInd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLvl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STNumberFormat;

import java.math.BigInteger;

public class NumberingFactory {
    private static BigInteger bulletPointsNumId = null;

    private NumberingFactory() {
    }

    public static synchronized BigInteger getBulletPoints(final XWPFDocument document) {
        if(bulletPointsNumId == null) {

            CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
            cTAbstractNum.setAbstractNumId(BigInteger.ONE);

            CTLvl cTLvl = cTAbstractNum.addNewLvl();
            cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
            cTLvl.addNewLvlText().setVal("•");
            cTLvl.setIlvl(BigInteger.ZERO);

            // set formatting to the left
            CTInd ctInd = CTInd.Factory.newInstance();
            ctInd.setHanging(BigInteger.valueOf(360));
            ctInd.setLeft(BigInteger.valueOf(720));

            CTPPr ctpPr = cTLvl.addNewPPr();
            ctpPr.setInd(ctInd);

            XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

            XWPFNumbering numbering = document.createNumbering();
            BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

            bulletPointsNumId = numbering.addNum(abstractNumID);
        }
        return bulletPointsNumId;
    }
}
