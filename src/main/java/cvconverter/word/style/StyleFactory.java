package cvconverter.word.style;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPBdr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STStyleType;

import java.math.BigInteger;
import java.util.EnumMap;
import java.util.Map;

public class StyleFactory {
    private static final Map<Style, CTStyle> NAME_TO_STYLE = new EnumMap<>(Style.class);

    private StyleFactory() {
    }

    // app does not use multithreading, however putting synchronized in case it will in the future
    public static synchronized String getStyleBody(final XWPFDocument document) {
        return createStyleIfNotPresent(document, Style.BODY);
    }

    // app does not use multithreading, however putting synchronized in case it will in the future
    public static synchronized String getStyleHeadingTopBorder(final XWPFDocument document) {
       return createStyleIfNotPresent(document, Style.HEADER_TOP_BORDER);
    }

    // app does not use multithreading, however putting synchronized in case it will in the future
    private static synchronized String createStyleIfNotPresent(final XWPFDocument document, Style style) {
        XWPFStyles styles = document.createStyles();
        NAME_TO_STYLE.computeIfAbsent(style, s -> createStyleNoBorder(styles, s));
        return style.name();
    }

    // app does not use multithreading, however putting synchronized in case it will in the future
    private static synchronized CTStyle createStyleNoBorder(final XWPFStyles styles,
                                                            final Style style) {
        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(style.name());

        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(style.name());
        ctStyle.setName(styleName);

        if(style.isHasTopBorder()) {
            CTPPr ppr = CTPPr.Factory.newInstance();
            CTBorder ctBorder = CTBorder.Factory.newInstance();
            ctBorder.setColor("auto");
            ctBorder.setSz(new BigInteger("4"));
            ctBorder.setSpace(new BigInteger("0"));
            ctBorder.setVal(STBorder.SINGLE);
            CTPBdr ctpBdr = CTPBdr.Factory.newInstance();
            ctpBdr.setTop(ctBorder);
            ppr.setPBdr(ctpBdr);
            ctStyle.setPPr(ppr);
        }

        XWPFStyle xwpfStyle = new XWPFStyle(ctStyle);
        xwpfStyle.setType(STStyleType.PARAGRAPH);
        styles.addStyle(xwpfStyle);
        return ctStyle;
    }
}
