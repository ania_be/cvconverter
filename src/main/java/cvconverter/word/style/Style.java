package cvconverter.word.style;

import lombok.Getter;

public enum Style {
    BODY(false),
    HEADER_TOP_BORDER(true);

    @Getter
    private final boolean hasTopBorder;

    Style(final boolean hasTopBorder) {
        this.hasTopBorder =  hasTopBorder;
    }
}
