package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import static cvconverter.model.CvParts.SHORT_SUMMARY;

public class ProfileFormatter extends Formatter<String> {

    @Override
    public void format(final XWPFDocument document, final String shortSummary) {
        createTitle(document, getHeader());
        XWPFParagraph paragraph = document.createParagraph();
        getXwpfRun(paragraph, TEXT_FONT_SIZE, shortSummary);
    }

    @Override
    String retrievePart(final InputCv inputCv) {
        return inputCv.getShortSummary();
    }

    @Override
    CvParts getCvPart() {
        return SHORT_SUMMARY;
    }
}
