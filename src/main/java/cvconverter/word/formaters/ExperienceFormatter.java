package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.Experience;
import cvconverter.model.InputCv;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.util.List;

import static cvconverter.model.CvParts.EXPERIENCE;

public class ExperienceFormatter extends SimpleListFormatter<List<Experience>> {

    public void format(final XWPFDocument document, final List<Experience> experiences) {
        createTitle(document, getHeader());
        experiences.forEach(experience -> createOneExperience(document, experience));
    }

    @Override
    List<Experience> retrievePart(final InputCv inputCv) {
        return inputCv.getExperiences();
    }

    @Override
    CvParts getCvPart() {
        return EXPERIENCE;
    }

    private void createOneExperience(final XWPFDocument document, final Experience experience) {
        XWPFParagraph paragraph = document.createParagraph();
        setStyleHeadingTopBorder(document, paragraph);
        getXwpfRun(paragraph, BIG_TEXT_FONT_SIZE, String.format("%s, %s", experience.getTitle(), experience.getDateCompanyLine()));
        paragraph = document.createParagraph();
        setStyleBody(document, paragraph);
        formatSimpleList(document, experience.getDescription());
        createEmpty(document);
    }
}
