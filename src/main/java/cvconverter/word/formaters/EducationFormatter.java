package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.Education;
import cvconverter.model.InputCv;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.util.List;

public class EducationFormatter extends Formatter<List<Education>> {

    @Override
    public void format(final XWPFDocument document, final List<Education> educationList) {
        createTitle(document, getHeader());
        educationList.forEach(edu -> createEducationEntry(document, edu));
    }

    @Override
    List<Education> retrievePart(final InputCv inputCv) {
        return inputCv.getEducation();
    }

    @Override
    CvParts getCvPart() {
        return CvParts.EDUCATION;
    }

    private void createEducationEntry(final XWPFDocument document, final Education edu) {
        XWPFParagraph paragraph = document.createParagraph();
        setStyleHeadingTopBorder(document, paragraph);
        final String text = formatEducationEntry(edu);
        getXwpfRun(paragraph, BIG_TEXT_FONT_SIZE, text);
        paragraph = document.createParagraph();
        setStyleBody(document, paragraph);
        getXwpfRun(paragraph, BIG_TEXT_FONT_SIZE, "\n");
    }

    private String formatEducationEntry(final Education education) {
        StringBuilder sb = new StringBuilder();
        education.getDescription().forEach(e -> {
            sb.append(e);
            sb.append(" ");
        });
        String eduBody = sb.toString().strip();
        return String.format("%s, %s - %s", eduBody, education.getStart(), education.getEnd());
    }
}
