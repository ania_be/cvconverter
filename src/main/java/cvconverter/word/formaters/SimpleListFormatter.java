package cvconverter.word.formaters;

import cvconverter.model.SimpleList;
import cvconverter.word.numbering.NumberingFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.util.List;

abstract class SimpleListFormatter<T> extends Formatter<T> {

    void formatSimpleList(final XWPFDocument document, final SimpleList simpleList) {
        formatSimpleList(document, simpleList, null);
    }

    void formatSimpleList(final XWPFDocument document, final SimpleList simpleList, final String title) {
        List<String> elements = simpleList.getElements();
        if (!CollectionUtils.isEmpty(elements)) {
            if (!StringUtils.isBlank(title)) {
                createTitle(document, String.format("\n%s\n", title));
            }
            elements.forEach(text -> addEntry(document, text, simpleList.getHasBulletPoints()));
        }
    }

    private void addEntry(final XWPFDocument document, final String text, final Boolean hasBulletPoints) {
        XWPFParagraph paragraph = document.createParagraph();
        getXwpfRun(paragraph, TEXT_FONT_SIZE, text);
        if (hasBulletPoints) {
            paragraph.setNumID(NumberingFactory.getBulletPoints(document));
        }
    }
}
