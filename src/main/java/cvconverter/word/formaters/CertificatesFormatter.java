package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class CertificatesFormatter extends SimpleListFormatter<SimpleList> {

    @Override
    public void format(final XWPFDocument document, final SimpleList certificates) {
        formatSimpleList(document, certificates, getHeader());
    }

    @Override
    SimpleList retrievePart(final InputCv inputCv) {
        return inputCv.getCertificates();
    }

    @Override
    CvParts getCvPart() {
        return CvParts.CERTIFICATES;
    }
}
