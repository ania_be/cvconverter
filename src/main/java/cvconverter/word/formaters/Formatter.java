package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.word.style.StyleFactory;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.util.Optional;

public abstract class Formatter<T> {

    private static final String FONT_NAME = "Helvetica";

    static final int TEXT_FONT_SIZE = 11;
    static final int BIG_TEXT_FONT_SIZE = 13;
    private static final int TITLE_FONT_SIZE = 16;
    static final int NAME_FONT_SIZE = 30;

    public void formatIfPresent(final XWPFDocument document, final InputCv inputCv) {
        Optional.ofNullable(retrievePart(inputCv)).ifPresentOrElse(part -> format(document, part),
                () -> System.out.println(String.format("[FORMATTING] Part %s not found", getCvPart())));
    }

    abstract T retrievePart(final InputCv inputCv);

    abstract void format(final XWPFDocument document, final T cvPart);

    abstract CvParts getCvPart();

    String getHeader() {
        return getCvPart().getHeader();
    }

    void setStyleHeadingTopBorder(XWPFDocument document, XWPFParagraph paragraph) {
        paragraph.setStyle(StyleFactory.getStyleHeadingTopBorder(document));
    }

    void setStyleBody(XWPFDocument document, XWPFParagraph paragraph) {
        paragraph.setStyle(StyleFactory.getStyleBody(document));
    }

    void createTitle(XWPFDocument document, String title) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.LEFT);
        getXwpfRun(paragraph, TITLE_FONT_SIZE, title, true);
    }

    void createEmpty(XWPFDocument document) {
        getXwpfRun(document.createParagraph(), BIG_TEXT_FONT_SIZE, "");
    }

    XWPFRun getXwpfRun(final XWPFParagraph paragraph,
                       final int fontSize,
                       final String text) {
        return getXwpfRun(paragraph, fontSize, text, false);
    }

    XWPFRun getXwpfRun(final XWPFParagraph paragraph,
                       final int fontSize,
                       final String text,
                       final boolean bold) {
        XWPFRun run = paragraph.createRun();
        run.setFontSize(fontSize);
        run.setFontFamily(FONT_NAME);
        run.setText(text);
        run.setBold(bold);
        return run;
    }
}
