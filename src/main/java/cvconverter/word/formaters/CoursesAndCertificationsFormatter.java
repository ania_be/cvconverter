package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class CoursesAndCertificationsFormatter extends SimpleListFormatter<SimpleList> {

    @Override
    void format(final XWPFDocument document, final SimpleList coursesAndCertifications) {
        formatSimpleList(document, coursesAndCertifications, getHeader());
    }

    @Override
    SimpleList retrievePart(final InputCv inputCv) {
        return inputCv.getCoursesAndCertifications();
    }

    @Override
    CvParts getCvPart() {
        return CvParts.COURSES_AND_CERTIFICATIONS;
    }
}
