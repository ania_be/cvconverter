package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import static cvconverter.model.CvParts.NAME;

public class NameFormatter extends Formatter<String> {

    @Override
    public void format(final XWPFDocument document, final String name) {
        // FIXME make sure that whole name is toLowerCase() and
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.LEFT);
        getXwpfRun(paragraph, NAME_FONT_SIZE, String.format("%s\n", name), true);
    }

    @Override
    String retrievePart(InputCv inputCv) {
        return inputCv.getName();
    }

    @Override
    CvParts getCvPart() {
        return NAME;
    }
}
