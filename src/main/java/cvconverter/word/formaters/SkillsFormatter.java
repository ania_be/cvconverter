package cvconverter.word.formaters;

import cvconverter.model.CvParts;
import cvconverter.model.InputCv;
import cvconverter.model.SimpleList;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import static cvconverter.model.CvParts.SKILLS;

public class SkillsFormatter extends SimpleListFormatter<SimpleList> {

    @Override
    public void format(final XWPFDocument document, final SimpleList skills) {
        formatSimpleList(document, skills, getHeader());
    }

    @Override
    SimpleList retrievePart(final InputCv inputCv) {
        return inputCv.getSkills();
    }

    @Override
    CvParts getCvPart() {
        return SKILLS;
    }
}
