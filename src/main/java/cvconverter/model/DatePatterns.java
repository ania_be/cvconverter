package cvconverter.model;

import lombok.Getter;

import java.util.regex.Pattern;

public enum DatePatterns {
    YEAR_ONLY(Pattern.compile("\\d{4}")),
    NUMERIC_MONTH_YEAR(Pattern.compile("\\d{2}\\.\\d{4}")),
    NUMERIC_YEAR_MONTH(Pattern.compile("\\d{4}\\.\\d{2}")),
    SPELLED_MONTH_YEAR(Pattern.compile("[a-zA-Z].+ \\d{4}"));

    @Getter
    private final Pattern datePattern;

    DatePatterns(Pattern datePattern) {
        this.datePattern =  datePattern;
    }
}
