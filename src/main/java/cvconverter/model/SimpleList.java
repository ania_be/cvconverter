package cvconverter.model;

import com.google.common.collect.ImmutableList;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class SimpleList {

    public static SimpleList EMPTY = SimpleList.builder()
            .elements(ImmutableList.of())
            .hasBulletPoints(false)
            .indexAfter(0)
            .build();

    private final List<String> elements;
    private final Boolean hasBulletPoints;
    private final Integer indexAfter;
}
