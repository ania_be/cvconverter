package cvconverter.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class InputCv {
    private String name;
    private String title;
    private String shortSummary;
    private List<Experience> experiences;
    private List<Education> education;
    private SimpleList skills;
    private SimpleList certificates;
    private SimpleList coursesAndCertifications;
}
