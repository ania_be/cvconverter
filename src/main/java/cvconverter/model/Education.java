package cvconverter.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Education {
    private String start;
    private String end;
    private List<String> description;
}
