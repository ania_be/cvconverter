package cvconverter.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Experience {
    private String title;
    private String dateCompanyLine;
    private SimpleList description;
}
