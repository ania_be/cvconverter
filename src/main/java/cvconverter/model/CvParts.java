package cvconverter.model;

import lombok.Getter;

@Getter
public enum CvParts {
    EXPERIENCE ("EXPERIENCE", "\nExperience\n"),
    EDUCATION("EDUCATION", "\nEducation\n"),
    SKILLS("SKILLS", "Skills"),
    CERTIFICATES("CERTIFICATES", "Certificates"),
    COURSES_AND_CERTIFICATIONS("COURSES AND CERTIFICATIONS", "Courses and certifications"),
    SHORT_SUMMARY("Short summary", "Profile"),
    NAME("name", "Name");

    private final String partName;
    private final String header;

    CvParts(final String partName, final String header) {
        this.partName = partName;
        this.header = header;
    }
}
