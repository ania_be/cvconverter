package cvconverter.util;

import cvconverter.model.SimpleList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public class LineReader {

    private static final String PREFIX_LISTING = "-";

    public static SimpleList read(final List<String> pdfLines,
                                  int currentIndex,
                                  final BiFunction<List<String>, Integer, Boolean> checkIfContinueProcessing) {
        List<String> elements = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        boolean hasBulletPoints = checkBulletPoints(pdfLines, currentIndex);
        while (checkIfContinueProcessing.apply(pdfLines, currentIndex)) {
            final String currentLine = pdfLines.get(currentIndex).stripLeading();
            if (currentLine.startsWith(PREFIX_LISTING)) {
                // found new bullet point:
                // - add data already collected in StringBuilder to the list
                // - create new String Builder and add bullet point as the first line
                addStringIfNotEmpty(elements, sb);
                sb = new StringBuilder();
                sb.append(String.format("%s\n", currentLine.substring(1).stripLeading()));
            } else {
                sb.append(String.format("%s\n", currentLine));
            }
            currentIndex++;
        }
        addStringIfNotEmpty(elements, sb);

        return SimpleList.builder()
                .hasBulletPoints(hasBulletPoints)
                .elements(elements)
                .indexAfter(currentIndex)
                .build();
    }

    private static Boolean checkBulletPoints(final List<String> pdfLines, final int currentIndex) {
        return pdfLines.get(currentIndex).stripLeading().startsWith(PREFIX_LISTING);
    }

    private static void addStringIfNotEmpty(final List<String> result, final StringBuilder sb) {
        String chunk = sb.toString().strip().replace('\n', ' ');
        if (!chunk.isEmpty()) {
            result.add(chunk);
        }
    }
}
